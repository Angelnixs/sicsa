//==============================================================================
package arcade.controller;
//==============================================================================

import arcade.data.EntradaDao;
//==============================================================================
import java.io.IOException;
import java.io.PrintWriter;
//==============================================================================
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//==============================================================================
/* @author Luis Cortez */
//==============================================================================

public class ManageEntrada extends HttpServlet {
//==============================================================================

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
//==============================================================================

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //======================================================================
        String accion = request.getParameter("accion");
        //======================================================================
        if ("select_entrada_tabla".equals(accion)) {
            String renglones = EntradaDao.select_entrada_tabla(
                    request.getParameter("fecha")
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("select_nombre_entrada".equals(accion)) {
            String renglones = EntradaDao.select_nombre_entrada(
                    Integer.parseInt(request.getParameter("id"))
                );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("insert_registro".equals(accion)) {
            String renglones = EntradaDao.insert_registro(
                    Integer.parseInt(request.getParameter("id"))
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("insert_registro_2".equals(accion)) {
            String renglones = EntradaDao.insert_registro_2(
                    Integer.parseInt(request.getParameter("id"))
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("update_entrada".equals(accion)) {
            String renglones = EntradaDao.update_entrada(
                    Integer.parseInt(request.getParameter("id_entrada"))
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("delete_entrada".equals(accion)) {
            String renglones = EntradaDao.delete_entrada(
                    Integer.parseInt(request.getParameter("id_entrada"))
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        else if ("select_hora_server".equals(accion)) {
            String renglones = EntradaDao.select_hora_server();
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
        if ("select_reporte_entradas".equals(accion)) {
            String renglones = EntradaDao.select_reporte_entradas(
                    request.getParameter("id_sucursal"),
                    request.getParameter("fecha")
            );
            response.setContentType("text/html; charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.print(renglones);
            writer.flush();
            writer.close();
        }//=====================================================================
    }
}
