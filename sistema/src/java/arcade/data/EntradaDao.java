package arcade.data;

import arcade.model.Entrada;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/* @author LUIS CORTEZ */
public class EntradaDao {

    //==========================================================================
    // nuevo query de entradas 
    public static String select_entrada_tabla(String fecha) {
        try {
            StartConn s = new StartConn();
            String sql = "call arcade_call.lista_entradas('" + fecha + "');";
            System.out.println(sql);
            String renglones;
            s.rs = s.st.executeQuery(sql);
            List<Entrada> entrada = new ArrayList<Entrada>();
            while (s.rs.next()) {
                Entrada c = new Entrada();
                // ESTOS SON LOS ENCABEZADOS DE LA COLUMNA DE LA TABLA EN MYSQL
                c.setTurno(s.rs.getString("descripcion"));
                c.setId_usuario(s.rs.getInt("id"));
                c.setUsuario(s.rs.getString("nombre"));
                c.setArea(s.rs.getString("area"));
                c.setSucursal(s.rs.getString("sucursal"));
                c.setEntrada(s.rs.getString("h_entrada"));
//                c.setSalida_comida(s.rs.getString("salida_comida"));
//                c.setEntrada_comida(s.rs.getString("entrada_comida"));
                c.setSalida(s.rs.getString("h_salida"));
//                c.setTotal(s.rs.getString("total"));
                c.setColor(s.rs.getString("status"));
                entrada.add(c);
            }
            
            //variables a usar
            int falta = 1;

            renglones = ""
                    + "<table id='tabla_entradas' class='striped';><thead><tr>"
                    // ESTOS SON LOS ENCABEZADOS DE LA COLUMNA DE LA TABLA
                    + "<th>TURNO</th>"
                    + "<th style='text-align:center;'>ID</th>"
                    + "<th style='text-align:left'>NOMBRE</th>"
                    + "<th style='text-align:left'>AREA</th>"
                    + "<th style='text-align:left;'>SUCURSAL</th>"
                    + "<th style='text-align:center;'>ENTRADA</th>"
//                    + "<th style='text-align:center;'>S.COMIDA</th>"
//                    + "<th style='text-align:center;'>E.COMIDA</th>"
                    + "<th style='text-align:center;'>SALIDA</th>"
//                    + "<th style='text-align:center;'>TOTAL</th>"
                    + "</tr></thead><tbody>";

            // CUEPRO DE LA TABLA
            for (Entrada c : entrada) {
                
                renglones += ""
                        + "<tr id='" + c.getId_usuario() + "' style='cursor: pointer; background-color:" + c.getColor() + ";'>"
                        + "<td>" + c.getTurno() + "</td>"
                        + "<td style='text-align:center;' >" + c.getId_usuario() + "</td>"
                        + "<td style='text-align:left;' >" + c.getUsuario() + "</td>"
                        + "<td style='text-align:left;' >" + c.getArea() + "</td>"
                        + "<td style='text-align:left;' >" + c.getSucursal() + "</td>"
                        + "<td>" + c.getEntrada() + "</td>"
//                        + "<td>" + c.getSalida_comida() + "</td>"
//                        + "<td>" + c.getEntrada_comida() + "</td>"
                        + "<td>" + c.getSalida() + "</td>"
//                        + "<td>" + c.getTotal() + "</td>"
                        + "</tr>";
            }
            renglones += "</tbody></table>";

           s.rs.close();
            s.st.close();
            s.conn.close();
            return renglones;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }
//    public static String select_entrada_tabla(String fecha) {
//        try {
//            StartConn s = new StartConn();
//            String sql = "call arcade_call.arcade_select_lista_entradas('" + fecha + "');";
//            System.out.println(sql);
//            String renglones;
//            s.rs = s.st.executeQuery(sql);
//            List<Entrada> entrada = new ArrayList<Entrada>();
//            while (s.rs.next()) {
//                Entrada c = new Entrada();
//                // ESTOS SON LOS ENCABEZADOS DE LA COLUMNA DE LA TABLA EN MYSQL
//                c.setTurno(s.rs.getString("turno"));
//                c.setId_usuario(s.rs.getInt("id"));
//                c.setUsuario(s.rs.getString("alias"));
//                c.setSucursal(s.rs.getString("sucursal"));
//                c.setEntrada(s.rs.getString("entrada"));
//                c.setSalida_comida(s.rs.getString("salida_comida"));
//                c.setEntrada_comida(s.rs.getString("entrada_comida"));
//                c.setSalida(s.rs.getString("salida"));
//                c.setTotal(s.rs.getString("total"));
//                c.setColor(s.rs.getString("color"));
//                entrada.add(c);
//            }
//
//            renglones = ""
//                    + "<table id='tabla_entradas' class='striped';><thead><tr>"
//                    // ESTOS SON LOS ENCABEZADOS DE LA COLUMNA DE LA TABLA
//                    + "<th>TURNO</th>"
//                    + "<th style='text-align:center;'>ID</th>"
//                    + "<th style='text-align:left'>NOMBRE</th>"
//                    + "<th style='text-align:left;'>SUCURSAL</th>"
//                    + "<th style='text-align:center;'>ENTRADA</th>"
//                    + "<th style='text-align:center;'>S.COMIDA</th>"
//                    + "<th style='text-align:center;'>E.COMIDA</th>"
//                    + "<th style='text-align:center;'>SALIDA</th>"
//                    + "<th style='text-align:center;'>TOTAL</th>"
//                    + "</tr></thead><tbody>";
//
//            // CUEPRO DE LA TABLA
//            for (Entrada c : entrada) {
//                renglones += ""
//                        + "<tr id='" + c.getId_usuario() + "' style='cursor: pointer; background-color:" + c.getColor() + ";'>"
//                        + "<td>" + c.getTurno() + "</td>"
//                        + "<td style='text-align:center;' >" + c.getId_usuario() + "</td>"
//                        + "<td style='text-align:left;' >" + c.getUsuario() + "</td>"
//                        + "<td style='text-align:left;' >" + c.getSucursal() + "</td>"
//                        + "<td>" + c.getEntrada() + "</td>"
//                        + "<td>" + c.getSalida_comida() + "</td>"
//                        + "<td>" + c.getEntrada_comida() + "</td>"
//                        + "<td>" + c.getSalida() + "</td>"
//                        + "<td>" + c.getTotal() + "</td>"
//                        + "</tr>";
//            }
//            renglones += "</tbody></table>";
//
//           s.rs.close();
//            s.st.close();
//            s.conn.close();
//            return renglones;
//        } catch (SQLException ex) {
//            return "SQL COde:" + ex;
//        }
//    }

    //==============================================================================
    public static String select_entrada_combo() {
        try {
            StartConn s = new StartConn();
            String sql = "call arcade_select_entrada();";
            String renglones = "";
            s.rs = s.st.executeQuery(sql);
            List<Entrada> entrada = new ArrayList<Entrada>();
            while (s.rs.next()) {
                Entrada e = new Entrada();
                e.setId_entrada(s.rs.getInt("id_entrada"));
                e.setEntrada(s.rs.getString("entrada"));
                entrada.add(e);
            }
            for (Entrada c : entrada) {
                renglones += "<option value='" + c.getId_entrada() + "'>" + c.getEntrada() + "</option>";
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return renglones;
        } catch (SQLException ex) {
            return "SQL Code: " + ex;
        }
    }

    //==========================================================================
    public static List<Entrada> select_entrada() {
        try {
            StartConn s = new StartConn();
            String sql = "call arcade_select_entrada();";
            s.rs = s.st.executeQuery(sql);
            List<Entrada> entrada = new ArrayList<Entrada>();
            while (s.rs.next()) {
                Entrada e = new Entrada();
                e.setId_entrada(s.rs.getInt("id_entrada"));
                e.setEntrada(s.rs.getString("entrada"));
                entrada.add(e);
            }

            s.rs.close();
            s.st.close();
            s.conn.close();
            return entrada;
        } catch (SQLException ex) {
            return null;
        }
    }

    //==========================================================================
    public static String insert_registro(int id) {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "call arcade_checar('" + id + "');";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("resultado"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }

    //==========================================================================
    public static String insert_registro_2(int id) {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "call arcade_checar_2('" + id + "');";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("resultado"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }
    //==========================================================================
    public static String select_nombre_entrada(int id) {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "call arcade_checar_1('" + id + "');";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("res"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }

    //==========================================================================
    public static String update_entrada(int id_entrada) {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "call arcade_update_entrada('" + id_entrada + "');";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("resultado"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }

    //==========================================================================
    public static String delete_entrada(int id_entrada) {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "call arcade_delete_entrada('" + id_entrada + "');";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("resultado"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }

    //==========================================================================
    public static String select_hora_server() {
        try {
            StartConn s = new StartConn();
            String resultado = "";
            String sql = "select curtime() as resultado;";
            s.rs = s.st.executeQuery(sql);
            while (s.rs.next()) {
                resultado = (s.rs.getString("resultado"));
            }
            s.rs.close();
            s.st.close();
            s.conn.close();
            return resultado;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }
    //==========================================================================
    public static String select_reporte_entradas(String id_sucursal,String fecha) {
        try {
            StartConn s = new StartConn();
            String sql = "call arcade_select_reporte_horarios('" + id_sucursal + "','" + fecha + "');";
            System.out.println(sql);
            String renglones;
            s.rs = s.st.executeQuery(sql);  
            List<Entrada> entrada = new ArrayList<Entrada>();
            while (s.rs.next()) {
                Entrada c = new Entrada();
                c.setId_usuario(s.rs.getInt("id_usuario"));
                c.setUsuario(s.rs.getString("nombre"));
                c.setEntrada(s.rs.getString("hora_entrada"));
                c.setDia(s.rs.getInt("dia"));
                c.setStatus_entrada(s.rs.getInt("status_entrada"));
                entrada.add(c);
            }
            int t_lista = 0, i=0,j=0, id=0, old_id = 0;
            for (Entrada c : entrada) {
                if (id == 0) {
                    id = c.getId_usuario();
                    old_id = c.getId_usuario();
                    i=1;
                }
                id = c.getId_usuario();
                if (id != old_id ) {
                    i++;
                    old_id =  c.getId_usuario();
                }
            }
            t_lista = i;
            String[][] datos_tabla = new String[t_lista][9];
            System.out.println("Longitud de la lista: " + t_lista);
            for(i=0;i < t_lista; i++){
                for(j=0;j < 9; j++){
                    datos_tabla[i][j] = "FALTA"; 
                }
            }
            renglones = ""
                    + "<table> <thead> <tr>"
                    + "<th>Id</th>"
                    + "<th>Nombre</th>"
                    + "<th>Lunes</th>"
                    + "<th>Martes</th>"
                    + "<th>Miercoles</th>"
                    + "<th>Jueves</th>"
                    + "<th>Viernes</th>"
                    + "<th>Sabado</th>"
                    + "</tr></thead>"
                    + "<tbody>";
            // VACIADO DE DATOS EN UN ARREGO DE 2 DIMENCIONES PARA SEPARAR LOS DATOS DE LOS DIAS DE LA SEMANA
            id=0;old_id = 0; i=0; j=0;
            for (Entrada c : entrada) {
                if (id == 0) {
                    datos_tabla[i][0] = "<td>"+Integer.toString(c.getId_usuario())+"</td>";
                    datos_tabla[i][1] = "<td>"+c.getUsuario()+"</td>";
                    datos_tabla[i][c.getDia()] = "<td>"+c.getEntrada()+"</td>";
                    
                    id = c.getId_usuario();
                    old_id = c.getId_usuario();
                    i=1;
                }
                // IDENTIFICADOR DE USUARIOS DIFERENTES
                id = c.getId_usuario();
                // ASIGNACION HORA DE ENTRADA DE CADA USUARIO
                datos_tabla[(i-1)][c.getDia()] = "<td style='background-color:"+(c.getStatus_entrada() == 1 ? "#00e676" : "#ffff00")+"' >"+c.getEntrada()+"</td>";
                if (id != old_id ) {
                    // ASIGANCION DE DATOS DE LOS DIFERENTES USUARIOS
                    i++;
                    old_id =  c.getId_usuario();
                    datos_tabla[(i-1)][0] = "<td>"+Integer.toString(c.getId_usuario())+"</td>";
                    datos_tabla[(i-1)][1] = "<td>"+c.getUsuario()+"</td>";
                }
                
            }
            
            for(i=0;i < t_lista; i++){
                renglones += "<tr>";
                for(j=0;j < 8; j++){
                    System.out.print(datos_tabla[i][j] + "   "); 
                    renglones += (datos_tabla[i][j].equals("FALTA") ? "<td style='background-color: #F44336;'>FALTA</td>" : datos_tabla[i][j] );
                }
                System.out.println("\n");
                renglones += "</tr>";
            }
            
            renglones += "</tbody></table>";

            s.rs.close();
            s.st.close();
            s.conn.close();
            return renglones;
        } catch (SQLException ex) {
            return "SQL COde:" + ex;
        }
    }

    //==========================================================================
}
