//==============================================================================
function select_solicitudes_tabla(_filtro, _id_caja) {
        var params = {
            accion: "select_solicitudes_tabla",
            filtro: _filtro
        };
        $.ajax({
            type: "POST",
            url: "/sistema/ManageSolicitud",
            data: params,
            dataType: "html",
            success: function (dataRes) {
                $("#" + _id_caja).empty();
                $("#" + _id_caja).append(dataRes);
            }
        });
    }
    ;
    //==============================================================================
    function insert_solicitud(
            _nombre,
            _puesto,
            _fecha,
            _exp,
            _sucursal,
            _medio,
            _reclutador,
            _calle,
            _tel,
            _cel,
            _colonia,
            _ciudad,
            _mail,
            _fecha_n,
            _lugar_n,
            _sexo,
            _edad,
            _num_imss,
            _clinica,
            _civil,
            _religion,
            _rfc,
            _dep_eco,
            _curp,
            _infonavit,
            _complexion,
            _edo_sal,
            _tipo_empleo,
            _cambio_res,
            _viajar,
            _auto,
            _lic,
            _extra,
            _tbj_act,
            _horario_1,
            _est_act,
            _horario_2,
            _call_center,
            _mora,
            _nivel,
            _grado,
            _idioma_1,
            _rango_1,
            _idioma_2,
            _rango_2,
            _software,
            _padre,
            _edad_padre,
            _domicilio_padre,
            _ocupacion_padre,
            _madre,
            _edad_madre,
            _domicilio_madre,
            _ocupacion_madre,
            _otro_1,
            _otro_1_edad,
            _otro_1_domicilio,
            _otro_1_ocupacion,
            _otro_2,
            _otro_2_edad,
            _otro_2_domicilio,
            _otro_2_ocupacion,
            _nombre_1,
            _giro_1,
            _area_1,
            _puesto_1,
            _domi_1,
            _dura_1,
            _mora_1,
            _tel_1,
            _jefe_1,
            _inicio_1,
            _termina_1,
            _sueldo_1,
            _comision_1,
            _producto_1,
            _motivo_1,
            _rh_1,
            _funcion_1,
            _tec_1,
            _nombre_2,
            _giro_2,
            _area_2,
            _puesto_2,
            _domi_2,
            _dura_2,
            _mora_2,
            _tel_2,
            _jefe_2,
            _inicio_2,
            _termina_2,
            _sueldo_2,
            _comision_2,
            _producto_2,
            _motivo_2,
            _rh_2,
            _funcion_2,
            _tec_2,
            _nombre_3,
            _giro_3,
            _area_3,
            _puesto_3,
            _domi_3,
            _dura_3,
            _mora_3,
            _tel_3,
            _jefe_3,
            _inicio_3,
            _termina_3,
            _sueldo_3,
            _comision_3,
            _producto_3,
            _motivo_3,
            _rh_3,
            _funcion_3,
            _tec_3,
            _virtud,
            _especificaMedio,
            _alias        
            ) {
        var params = {
            accion: "insert_solicitud",
            nombre: _nombre,
            puesto: _puesto,
            fecha: _fecha,
            exp: _exp,
            sucursal: _sucursal,
            medio: _medio,
            reclutador: _reclutador,
            calle: _calle,
            tel: _tel,
            cel: _cel,
            colonia: _colonia,
            ciudad: _ciudad,
            mail: _mail,
            fecha_n: _fecha_n,
            lugar_n: _lugar_n,
            sexo: _sexo,
            edad: _edad,
            num_imss: _num_imss,
            clinica: _clinica,
            civil: _civil,
            religion: _religion,
            rfc: _rfc,
            dep_eco: _dep_eco,
            curp: _curp,
            infonavit: _infonavit,
            complexion: _complexion,
            edo_sal: _edo_sal,
            tipo_empleo: _tipo_empleo,
            cambio_res: _cambio_res,
            viajar: _viajar,
            auto: _auto,
            lic: _lic,
            extra: _extra,
            tbj_act: _tbj_act,
            horario_1: _horario_1,
            est_act: _est_act,
            horario_2: _horario_2,
            call_center: _call_center,
            mora: _mora,
            nivel: _nivel,
            grado: _grado,
            idioma_1: _idioma_1,
            rango_1: _rango_1,
            idioma_2: _idioma_2,
            rango_2: _rango_2,
            software: _software,
            padre: _padre,
            edad_padre: _edad_padre,
            domicilio_padre: _domicilio_padre,
            ocupacion_padre: _ocupacion_padre,
            madre: _madre,
            edad_madre: _edad_madre,
            domicilio_madre: _domicilio_madre,
            ocupacion_madre: _ocupacion_madre,
            otro_1: _otro_1,
            otro_1_edad: _otro_1_edad,
            otro_1_domicilio: _otro_1_domicilio,
            otro_1_ocupacion: _otro_1_ocupacion,
            otro_2: _otro_2,
            otro_2_edad: _otro_2_edad,
            otro_2_domicilio: _otro_2_domicilio,
            otro_2_ocupacion: _otro_2_ocupacion,
            nombre_1: _nombre_1,
            giro_1: _giro_1,
            area_1: _area_1,
            puesto_1: _puesto_1,
            domi_1: _domi_1,
            dura_1: _dura_1,
            mora_1: _mora_1,
            tel_1: _tel_1,
            jefe_1: _jefe_1,
            inicio_1: _inicio_1,
            termina_1: _termina_1,
            sueldo_1: _sueldo_1,
            comision_1: _comision_1,
            producto_1: _producto_1,
            motivo_1: _motivo_1,
            rh_1: _rh_1,
            funcion_1: _funcion_1,
            tec_1: _tec_1,
            nombre_2: _nombre_2,
            giro_2: _giro_2,
            area_2: _area_2,
            puesto_2: _puesto_2,
            domi_2: _domi_2,
            dura_2: _dura_2,
            mora_2: _mora_2,
            tel_2: _tel_2,
            jefe_2: _jefe_2,
            inicio_2: _inicio_2,
            termina_2: _termina_2,
            sueldo_2: _sueldo_2,
            comision_2: _comision_2,
            producto_2: _producto_2,
            motivo_2: _motivo_2,
            rh_2: _rh_2,
            funcion_2: _funcion_2,
            tec_2: _tec_2,
            nombre_3: _nombre_3,
            giro_3: _giro_3,
            area_3: _area_3,
            puesto_3: _puesto_3,
            domi_3: _domi_3,
            dura_3: _dura_3,
            mora_3: _mora_3,
            tel_3: _tel_3,
            jefe_3: _jefe_3,
            inicio_3: _inicio_3,
            termina_3: _termina_3,
            sueldo_3: _sueldo_3,
            comision_3: _comision_3,
            producto_3: _producto_3,
            motivo_3: _motivo_3,
            rh_3: _rh_3,
            funcion_3: _funcion_3,
            tec_3: _tec_3,
            virtud: _virtud,
            especificaMedio: _especificaMedio,
            alias: _alias
        };
        console.log(params);
        $.ajax({
            type: "POST",
            url: "/sistema/ManageSolicitud",
            data: params,
            dataType: "html",
            success: function (dataRes) {
                alert(dataRes);
            }
        });
    }
    ;
    
    //=============================================================================
    function partir_datos(dataRes, _campo, _forlimit) {
        var campos = dataRes.split("#$");
        console.log(campos);
        for (i = 1; i <= _forlimit; i++) {
            console.log("#" + _campo + i + " valor { "+ campos[i] + " }");
            $("#" + _campo + i).empty();
            $("#" + _campo + i).val(campos[i]);
        }


        $("#evaluador_rh").empty();
        $("#evaluador_rh").val(campos[133]);
        $("#anexo").empty();
        $("#anexo").val(campos[134]);
        $("#anexo_rh").empty();
        $("#anexo_rh").val(campos[135]);


        $("#" + _campo + 2).empty();
        $("#" + _campo + 2).append('<option value="' + campos[2] + '">' + campos[2] + '</option>' +
                '<option value="Ninguno">Ninguno</option>' +
                '<option value="Gestor">Gestor</option>' +
                '<option value="Reclutador">Reclutador</option>' +
                '<option value="Supervisor">Supervisor</option>' +
                '<option value="Otros">Otros</option>' 
                );
        
        $("#" + _campo + 4).empty();
        $("#" + _campo + 4).append('<option value="' + campos[4] + '">' + campos[4] + '</option>' +
                '<option value="Ninguno">Ninguno</option>' +
                '<option value="Predictivo">Predictivo</option>' +
                '<option value="Manual">Manual</option>' +
                '<option value="Ambos">Ambos</option>'
                );
    
        $("#" + _campo + 5).empty();
        $("#" + _campo + 5).append('<option value="' + campos[5] + '">' + campos[5] + '</option>' +
                '<option value="Publicidad Fija">Publicidad Fija</option>' +
				'<option value="Bolsas Electronicas">Bolsas Electronicas</option>' +
				'<option value="Trabajo en Campo">Trabajo en Campo</option>' +
				'<option value="Redes Sociales">Redes Sociales</option>' +
				'<option value="Otros">Otros</option>' +
				'<option value="Escuelas">Escuelas</option>' +
				'<option value="Periodico">Periodico</option>' +
				'<option value="Bolsas Municipales">Bolsas Municipales</option>' +
				'<option value="Intercambio de Bolsas">Intercambio de Bolsas</option>'
                );
        $("#" + _campo + 131).empty();
        $("#" + _campo + 131).append('<option value="' + campos[131] + '">' + campos[131] + '</option>' 
                );
     
        $("#" + _campo + 15).empty();
        $("#" + _campo + 15).append('<option value="' + campos[15] + '">' + campos[15] + '</option>' +
                '<option value="Hombre">Hombre</option>' +
                '<option value="Mujer">Mujer</option>'
                );
    
        $("#" + _campo + 19).empty();
        $("#" + _campo + 19).append('<option value="' + campos[19] + '">' + campos[19] + '</option>' +
                '<option value="Soltero(a)">Soltero(a)</option>' +
                '<option value="Casado(a)">Casado(a)</option>' +
                '<option value="Union Libre">Union Libre</option>' +
                '<option value="Divorciado(a)">Divorciado(a)</option>' +
                '<option value="Viudo(a)">Viudo(a)</option>'
                );
    
        $("#" + _campo + 20).empty();
        $("#" + _campo + 20).append('<option value="' + campos[20] + '">' + campos[20] + '</option>' +
                '<option value="Catolica">Catolica</option>' +
                '<option value="Cristiana">Cristiana</option>' +
                '<option value="Testigo de Jehova">Testigo de Jehova</option>' +
                '<option value="Mormones">Mormones</option>' +
                '<option value="Ateo">Ateo</option>' +
                '<option value="Otro">Otro</option>'
                );
    
        $("#" + _campo + 25).empty();
        $("#" + _campo + 25).append('<option value="' + campos[25] + '">' + campos[25] + '</option>' +
                '<option value="Delgada">Delgada</option>' +
                '<option value="Media">Media</option>' +
                '<option value="Robusta">Robusta</option>'
                );
    
        $("#" + _campo + 26).empty();
        $("#" + _campo + 26).append('<option value="' + campos[26] + '">' + campos[26] + '</option>' +
                '<option value="Bueno">Bueno</option>' +
                ' <option value="Malo">Malo</option>' +
                '<option value="Regular">Regular</option>'
                );
    
        $("#" + _campo + 27).empty();
        $("#" + _campo + 27).append('<option value="' + campos[27] + '">' + campos[27] + '</option>' +
                '<option value="Completo">Completo</option>' +
                '<option value="Parcial">Parcial</option>' +
                '<option value="Rotativo">Rotativo</option>' +
                '<option value="Fijo">Fijo</option>'
                );
    
        $("#" + _campo + 28).empty();
        $("#" + _campo + 28).append('<option value="' + campos[28] + '">' + campos[28] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 29).empty();
        $("#" + _campo + 29).append('<option value="' + campos[29] + '">' + campos[29] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 30).empty();
        $("#" + _campo + 30).append('<option value="' + campos[30] + '">' + campos[30] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 31).empty();
        $("#" + _campo + 31).append('<option value="' + campos[31] + '">' + campos[31] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 33).empty();
        $("#" + _campo + 33).append('<option value="' + campos[33] + '">' + campos[33] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 35).empty();
        $("#" + _campo + 35).append('<option value="' + campos[35] + '">' + campos[35] + '</option>' +
                '<option value="Si">Si</option>' +
                '<option value="No">No</option>'
                );
    
        $("#" + _campo + 37).empty();
        $("#" + _campo + 37).append('<option value="' + campos[37] + '">' + campos[37] + '</option>' +
                '<option value="Cobranza">Cobranza</option>' +
                '<option value="Atencion a Clientes">Atencion a Clientes</option>' +
                '<option value="Ventas">Ventas</option>' +
                '<option value="Ninguno">Ninguno</option>'
                );
    
        $("#" + _campo + 38).empty();
        $("#" + _campo + 38).append('<option value="' + campos[38] + '">' + campos[38] + '</option>' +
                '<option value="Ninguna">Ninguna</option>' +
                '<option value="30">30</option>' +
                '<option value="60">60</option>' +
                '<option value="90">90</option>' +
                '<option value="120">120</option>' +
                '<option value="Mas">Mas</option>'
                );
    
        $("#" + _campo + 39).empty();
        $("#" + _campo + 39).append('<option value="' + campos[39] + '">' + campos[39] + '</option>' +
                '<option value="Ninguna">Ninguna</option>' +
                '<option value="Primaria">Primaria</option>' +
                '<option value="Secundaria">Secundaria</option>' +
                '<option value="Preparatoria">Preparatoria</option>' +
                '<option value="Tecnica">Tecnica</option>' +
                '<option value="Profesional">Profesional</option>'
                );
    
        $("#" + _campo + 116).empty();
        $("#" + _campo + 116).append('<option value="' + campos[116] + '">' + campos[116] + '</option>' +
                '<option value="Monterrey">Monterrey</option>' +
                '<option value="Puebla">Puebla</option>'
                );
        $("#presentacion").empty();
        $("#presentacion").append('<option value="' + campos[117] + '">' + campos[117] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#experiencia").empty();
        $("#experiencia").append('<option value="' + campos[118] + '">' + campos[118] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#facilidad").empty();
        $("#facilidad").append('<option value="' + campos[119] + '">' + campos[119] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#normas").empty();
        $("#normas").append('<option value="' + campos[120] + '">' + campos[120] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#influencia").empty();
        $("#influencia").append('<option value="' + campos[121] + '">' + campos[121] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#equipo").empty();
        $("#equipo").append('<option value="' + campos[122] + '">' + campos[122] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#estabilidad").empty();
        $("#estabilidad").append('<option value="' + campos[123] + '">' + campos[123] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#logro").empty();
        $("#logro").append('<option value="' + campos[124] + '">' + campos[124] + '</option>' +
                '<option value="Mal">Mal</option>' +
                '<option value="Regular">Regular</option>' +
                '<option value="Bien">Bien</option>' +
                '<option value="Muy Bien">Muy Bien</option>'
                );
    
        $("#contratacion").empty();
        $("#contratacion").append('<option value="' + campos[125] + '">' + campos[125] + '</option>' + '<option value="Aprobado">Aprobado</option>' +
                '<option value="Caido">Caido</option>' +
                '<option value="Stand By">Stand By</option>' +
                '<option value="No Aplica">No Aplica</option>'
                );
    
        $("#reclutador_rh").empty();
        $("#reclutador_rh").val(campos[127]);

        $("#dato142").empty();
        $("#dato142").append('<option value="' + campos[130] + '">' + campos[130] + '</option>');
        
        
    
        $('select').material_select();
        
    }
    ;
    //==============================================================================
    function select_datos_solicitud(_id_solicitud) {
        var params = {
            accion: "select_datos_solicitud",
            id_solicitud: _id_solicitud
        };
        console.log(params);
        $.ajax({
            type: "POST",
            url: "/sistema/ManageSolicitud",
            data: params,
            dataType: "html",
            success: function (dataRes) {
                partir_datos(dataRes, "dato", 132);
            }
        });
    }
    ;
    //==============================================================================
       //==============================================================================
 function enviar_evaluacion(
        _dato1,
        _dato2,
        _dato3,
        _dato4,
        _dato5,
        _dato6,
        _dato7,
        _dato8,
        _dato9,
        _dato10,
        _dato11,
        _dato12,
        _dato13,
        _dato14,
        _dato15,
        _dato16,
        _dato17,
        _dato18,
        _dato19,
        _dato20,
        _dato21,
        _dato22,
        _dato23,
        _dato24,
        _dato25,
        _dato26,
        _dato27,
        _dato28,
        _dato29,
        _dato30,
        _dato31,
        _dato32,
        _dato33,
        _dato34,
        _dato35,
        _dato36,
        _dato37,
        _dato38,
        _dato39,
        _dato40,
        _dato41,
        _dato42,
        _dato43,
        _dato44,
        _dato45,
        _dato46,
        _dato47,
        _dato48,
        _dato49,
        _dato50,
        _dato51,
        _dato52,
        _dato53,
        _dato54,
        _dato55,
        _dato56,
        _dato57,
        _dato58,
        _dato59,
        _dato60,
        _dato61,
        _dato62,
        _dato63,
        _dato64,
        _dato65,
        _dato66,
        _dato67,
        _dato68,
        _dato69,
        _dato70,
        _dato71,
        _dato72,
        _dato73,
        _dato74,
        _dato75,
        _dato76,
        _dato77,
        _dato78,
        _dato79,
        _dato80,
        _dato81,
        _dato82,
        _dato83,
        _dato84,
        _dato85,
        _dato86,
        _dato87,
        _dato88,
        _dato89,
        _dato90,
        _dato91,
        _dato92,
        _dato93,
        _dato94,
        _dato95,
        _dato96,
        _dato97,
        _dato98,
        _dato99,
        _dato100,
        _dato101,
        _dato102,
        _dato103,
        _dato104,
        _dato105,
        _dato106,
        _dato107,
        _dato108,
        _dato109,
        _dato110,
        _dato111,
        _dato112,
        _dato113,
        _dato114,
        _dato115,
        _dato116,
        _id_seleccion, _presentacion, _experiencia, _facilidad, _normas, _influencia, _equipo, _estabilidad, _logro, _contratacion, _fecha_ingreso, _nombre_reclutador, _dato128, _dato129, _dato130, _dato131, _dato132, _evaluador_rh, _anexo, _anexo_rh) {
    var params = {
        accion: "enviar_evaluacion",
        dato1: _dato1,
        dato2: _dato2,
        dato3: _dato3,
        dato4: _dato4,
        dato5: _dato5,
        dato6: _dato6,
        dato7: _dato7,
        dato8: _dato8,
        dato9: _dato9,
        dato10: _dato10,
        dato11: _dato11,
        dato12: _dato12,
        dato13: _dato13,
        dato14: _dato14,
        dato15: _dato15,
        dato16: _dato16,
        dato17: _dato17,
        dato18: _dato18,
        dato19: _dato19,
        dato20: _dato20,
        dato21: _dato21,
        dato22: _dato22,
        dato23: _dato23,
        dato24: _dato24,
        dato25: _dato25,
        dato26: _dato26,
        dato27: _dato27,
        dato28: _dato28,
        dato29: _dato29,
        dato30: _dato30,
        dato31: _dato31,
        dato32: _dato32,
        dato33: _dato33,
        dato34: _dato34,
        dato35: _dato35,
        dato36: _dato36,
        dato37: _dato37,
        dato38: _dato38,
        dato39: _dato39,
        dato40: _dato40,
        dato41: _dato41,
        dato42: _dato42,
        dato43: _dato43,
        dato44: _dato44,
        dato45: _dato45,
        dato46: _dato46,
        dato47: _dato47,
        dato48: _dato48,
        dato49: _dato49,
        dato50: _dato50,
        dato51: _dato51,
        dato52: _dato52,
        dato53: _dato53,
        dato54: _dato54,
        dato55: _dato55,
        dato56: _dato56,
        dato57: _dato57,
        dato58: _dato58,
        dato59: _dato59,
        dato60: _dato60,
        dato61: _dato61,
        dato62: _dato62,
        dato63: _dato63,
        dato64: _dato64,
        dato65: _dato65,
        dato66: _dato66,
        dato67: _dato67,
        dato68: _dato68,
        dato69: _dato69,
        dato70: _dato70,
        dato71: _dato71,
        dato72: _dato72,
        dato73: _dato73,
        dato74: _dato74,
        dato75: _dato75,
        dato76: _dato76,
        dato77: _dato77,
        dato78: _dato78,
        dato79: _dato79,
        dato80: _dato80,
        dato81: _dato81,
        dato82: _dato82,
        dato83: _dato83,
        dato84: _dato84,
        dato85: _dato85,
        dato86: _dato86,
        dato87: _dato87,
        dato88: _dato88,
        dato89: _dato89,
        dato90: _dato90,
        dato91: _dato91,
        dato92: _dato92,
        dato93: _dato93,
        dato94: _dato94,
        dato95: _dato95,
        dato96: _dato96,
        dato97: _dato97,
        dato98: _dato98,
        dato99: _dato99,
        dato100: _dato100,
        dato101: _dato101,
        dato102: _dato102,
        dato103: _dato103,
        dato104: _dato104,
        dato105: _dato105,
        dato106: _dato106,
        dato107: _dato107,
        dato108: _dato108,
        dato109: _dato109,
        dato110: _dato110,
        dato111: _dato111,
        dato112: _dato112,
        dato113: _dato113,
        dato114: _dato114,
        dato115: _dato115,
        dato116: _dato116,
        id_solicitud: _id_seleccion,
        presentacion: _presentacion,
        experiencia: _experiencia,
        facilidad: _facilidad,
        normas: _normas,
        influencia: _influencia,
        equipo: _equipo,
        estabilidad: _estabilidad,
        logro: _logro,
        contratacion: _contratacion,
        fecha_ingreso: _fecha_ingreso,
        nombre_reclutador: _nombre_reclutador,
        referido: _dato128,
        observacion: _dato129,
        virtud: _dato131,
        especificaMedio: _dato130,
        alias: _dato132,
        evaluador_rh: _evaluador_rh,
        anexo: _anexo,
        anexo_rh: _anexo_rh
    };
    console.log("DATOS ENVIADOS PARA HACER EL UPDATE");
    console.log(params);
    $.ajax({
        type: "POST",
        url: "/sistema/ManageSolicitud",
        data: params,
        dataType: "html",
        success: function (dataRes) {
            $("#div_solicitudes").show();
            $("#div_evaluacion").hide();
            alert(dataRes);
            select_solicitudes_tabla('', "listado_solicitudes");
        }
    });
}
;
    //==============================================================================
	function usuarios_datos(_nombre, _alias, _sucursal, _sexo, _tel, _cel, _mail, _codigo_reclutador) {
        var params = {
            accion: "insert_nuevo_usuario",
            nombre_usu: _nombre,
            alias_usu: _alias,
            sucursal_usu: _sucursal,
            sexo_usu: _sexo,
            tel_usu: _tel,
            cel_usu: _cel,
            mail_usu: _mail,
            codigo_reclutador: _codigo_reclutador
        };
        console.log(params);
        $.ajax({
            type: "POST",
            url: "/sistema/ManageSolicitud",
            data: params,
            dataType: "html",
            success: function (dataRes) {
                if(dataRes == ''){
                    alert("errorrrrrr");
                } else {
                    $("#idUsu").text(dataRes);
                }
            }
        });
    }
    ;
     //==============================================================================
function update_chunnun(_idusu,_acta, _ife, _nss, _curp1, _comp_est, _comp_dom, _cartas, _fotos, _infonavit1) {
    console.log(_idusu,_acta, _ife, _nss, _curp1, _comp_est, _comp_dom, _cartas, _fotos, _infonavit1);
    var params = {
        accion: "update_chunnun",
        idusu:_idusu,
        acta:_acta,
        ife:_ife,
        nss:_nss,
        curp1:_curp1,
        comp_est:_comp_est,
        comp_dom:_comp_dom,
        cartas:_cartas,
        fotos:_fotos,
        infonavit1:_infonavit1 
 
    };
    console.log(params);
    $.ajax({
        type: "POST",
        url: "/sistema/ManageUsuario",
        data: params,
        dataType: "html",
        success: function (dataRes) {
            
        }
    });
};    
//==============================================================================
function datos_grafica_dashboart() {

    var params = {
        accion: "datos_grafica_dashboart"
    };
    
    $.ajax({
        type: "POST",
        url: "/sistema/ManageSolicitud",
        data: params,
        dataType: "html",
        success: function (dataRes) {
            datos_graficas(dataRes);
        }
    });
};
//==============================================================================
function datos_grafica_dashboart_mensual(_mes, _desde,_hasta) {
    if (_mes == 0) {
        var params = {
            accion: "datos_grafica_dashboart2",
            mes: _mes,
            desde: _desde,
            hasa: _hasta
        };
        
    }else{
        var params = {
            accion: "datos_grafica_dashboart2",
            mes: _mes,
            dese: _desde,
            hasta: _hasta
        };
    }
    
    $.ajax({
        type: "POST",
        url: "/sistema/ManageSolicitud",
        data: params,
        dataType: "html",
        success: function (dataRes) {
            datos_graficas(dataRes);
        }
    });
};
//==============================================================================
function datos_graficas(dataRes){
    console.log(dataRes);
            datos = dataRes.split(",#");
            var datos_para_grafica = [];

            console.log(datos);
            for (var i = 0; i < datos.length - 1; i++) {
                datos_para_grafica[i] = datos[i].split(",");
            }
            console.log(datos_para_grafica);

            for (var i = 1; i < datos.length - 1; i++) {
                datos_para_grafica[i] = datos_para_grafica[i].map(Number);
            }
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Entrevistas'
                },
                subtitle: {
                    text: '<a id="1" class="waves-effect waves-light btn-flat chi">Ene</a>\n\
                           <a id="2" class="waves-effect waves-light btn-flat chi">Feb</a>\n\
                           <a id="3" class="waves-effect waves-light btn-flat chi">Mar</a>\n\
                           <a id="4" class="waves-effect waves-light btn-flat chi">Abr</a>\n\
                           <a id="5" class="waves-effect waves-light btn-flat chi">May</a>\n\
                           <a id="6" class="waves-effect waves-light btn-flat chi">Jun</a>\n\
                           <a id="7" class="waves-effect waves-light btn-flat chi">Jul</a>\n\
                           <a id="8" class="waves-effect waves-light btn-flat chi">Ago</a>\n\
                           <a id="9" class="waves-effect waves-light btn-flat chi">Sep</a>\n\
                           <a id="10" class="waves-effect waves-light btn-flat chi">Oct</a>\n\
                           <a id="11" class="waves-effect waves-light btn-flat chi">Nov</a>\n\
                           <a id="12" class="waves-effect waves-light btn-flat chi">Dic</a>', 
                    useHTML: true
                },
                xAxis: {
                    categories: datos_para_grafica[0], // yo modifique a qui emmanuel
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Solicitudes'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'Aprobado',
                        data: datos_para_grafica[1]

                    }, {
                        name: 'Caido',
                        data: datos_para_grafica[2]

                    }, {
                        name: 'No Aplica',
                        data: datos_para_grafica[3]

                    }, {
                        name: 'No Evaluado',
                        data: datos_para_grafica[4]

                    }, {
                        name: 'Stand By',
                        data: datos_para_grafica[5]

                    }]
            });
    
};