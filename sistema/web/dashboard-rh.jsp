<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ERP</title>


        <!--ICONS PARA MATERIALIZE-->
        <link rel="shortcut icon" href="http://leimihost.com/mx/images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="css/arcade-style-frame.css" media="screen">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--ICONS PARA MATERIALIZE-->

        <!--ARCADE CSS-->
        <!--        <link type="text/css" rel="stylesheet" href="css/arcade-responsive-default.css">
                <link type="text/css" rel="stylesheet" href="css/arcade-responsive-740.css">
       
                <link type="text/css" rel="stylesheet" href="styles/bootstrap.min.css">
                <link type="text/css" rel="stylesheet" href="styles/all.css">
                <link type="text/css" rel="stylesheet" href="styles/main.css">-->
        <!--ARCADE CSS-->

        <!-- INICIA CALENDARIO -->
        <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>       
        <link rel="stylesheet" href="datepicker/jquery-ui.css">
        <script src="datepicker/jquery-ui.js"></script>
        <!--    <script>
                    $(function () {
                        $(".datepicker").datepicker({changeMonth: true, changeYear: true, numberOfMonths: 1});
                    });
                </script>-->
        <!--TERMINA CALENDARIO -->

        <!--ARCADE JS-->
        <script type="text/javascript" src="js/arcade-fecha.js"></script>
        <script type="text/javascript" src="js/arcade-alfanumerico.js"></script>
        <script type="text/javascript" src="js/arcade-curp.js"></script>
        <!--ARCADE JS-->

        <!--MATERIALIZE-->
        <script type="text/javascript" src="materialize/js/materialize.js"></script>
        <link rel="stylesheet" type="text/css" href="materialize/css/materialize.css">
        <link rel="stylesheet" type="text/css" href="css/arcade-nav.css">
        <!--MATERIALIZE-->

        <!-- HIGHCHARTS-->
        <script type="text/javascript" src="highcharts/highcharts.js"></script>
        <script type="text/javascript" src="highcharts/pareto.js"></script> 
        <script type="text/javascript" src="highcharts/exporting.js"></script>
        <script type="text/javascript" src="js/arcade-solicitud.js"></script>
        <!-- HIGHCHARTS-->

        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <style>
            html {
                height: 100%;
            }
            body {
                background:#ffffff;
                /*                min-height: 100%;*/
            }


            /*             label color
                        .input-field label {
                            color: #000;
                        }
                         label focus color
                        .input-field input[type=text]:focus + label {
                            color: #000;
                        }
                         label underline focus color
                        .input-field input[type=text]:focus {
                            border-bottom: 1px solid #000;
                            box-shadow: 0 1px 0 0 #000;
                        }
                         valid color
                        .input-field input[type=text].valid {
                            border-bottom: 1px solid #000;
                            box-shadow: 0 1px 0 0 #000;
                        }
                         invalid color
                        .input-field input[type=text].invalid {
                            border-bottom: 1px solid #000;
                            box-shadow: 0 1px 0 0 #000;
                        }
                         icon prefix focus color
                        .input-field .prefix.active {
                            color: #000;
                        }*/
            .highcharts-background {
                fill:rgba(255, 255, 255, 0.2);
                stroke:rgba(0, 0, 0, 0.2);
                stroke-width:0px;

            }
            body{color:white;}
            .chi{
                padding: 0px 8px;
            }
        </style>

    </head>
    <body>
        <!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div class="container-fluid">
            <div class="row">
                <nav id="ayuda_nav" class="grey lighten-1 hide_print" >
                    <div class="nav-wrapper">
                        <div>
                            <a class="ayuda_name brand-logo" style="color:white;">Entradas</a>
                        </div>
                        <ul class="right"  style="margin-right:2%;">   

                            <li><i class="medium material-icons" style="margin-top: -13px;">access_time</i></li>
                            <li><input id="desde" name="fecha" type="text" class="validate datepicker" readonly="" style="text-align: center;border: solid 1px gray !important;border-radius: 4px !important;font-weight: bold; width:100px; padding:-5px;margin-bottom: -5px;height: 27px;margin-left: 10px;background: white;"></li>
                            <li style="margin-left: 10px;margin-top: -13px;"><i class="medium material-icons">access_time</i></li>
                            <li><input id="hasta" name="fecha" type="text" class="validate datepicker" readonly="" style="text-align: center;border: solid 1px gray !important;border-radius: 4px !important;font-weight: bold; width:100px; padding:-5px;margin-bottom: -5px;height: 27px;margin-right: 100px;margin-left: 10px;background: white;"></li>
                            <li id="pdf_horas-clase-maestro" style="margin-left: 117px;"><a href="#"><i id="nav_i" class="material-icons help">picture_as_pdf</i></a></li>
                            <li id="roboto">Manuales de usuario de este modulo</li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="row" style="margin-left:3%; margin-right:3%;">

                <div class="col s12">
                    <div class="col s3 z-depth-3 " style="background:linear-gradient(to left, rgba(30,235,46,0.5), rgba(30,235,46,0.9));border-radius: 3px;   padding-right: 18px; padding-left: 18px;width: 22.75%;"> 
                        <h4>Ingresos</h4>
                        <h3 id="ingreso" class="center-align">14</h3>
                        <h5 class="right-align">usuarios</h5>
                    </div>
                    <div class="col s3  z-depth-3 " style="background:linear-gradient(to left, rgba(62,123,255,0.5), rgba(62,123,255,0.9));border-radius: 3px;margin: 0px 38px 0px 38px;padding-right: 18px; padding-left: 18px;width: 22.75%;"> 
                        <h4>Activos</h4>
                        <h3 id="activo" class="center-align">123</h3>
                        <h5 class="right-align">usuarios</h5>
                    </div>
                    <div class="col s3 z-depth-3 " style=" background:linear-gradient(to left, rgba(255,0,0,0.5), rgba(255,0,0,0.9));border-radius: 3px; padding-right: 18px; padding-left: 18px;width: 22.75%;"> 
                        <h4>Bajas</h4>
                        <h3 id="baja" class="center-align">4</h3>
                        <h5 class="right-align">usuarios</h5>
                    </div>
                    <div class="col s3 z-depth-3 " style=" background:linear-gradient(to left, rgba(247,123,0,0.5), rgba(247,123,0,0.9));border-radius: 3px; padding-right: 18px; padding-left: 18px;width: 22.75%;margin: 0px 0px 0px 38px;"> 
                        <h4>Rotacion</h4>
                        <h3 id="baja" class="center-align">50</h3>
                        <h5 class="right-align">porciento</h5>
                    </div>
                    <div class="col s5 z-depth-3"  style="min-width: 310px; height: 400px;border-radius: 3px;margin-top: 20px;width:40.667%;margin-right: 12px;">
                       <div id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                    </div>
                    <div class="col s7 z-depth-3" id="container" style="min-width: 310px; height: 400px; margin: 0 auto;border-radius: 3px;margin-top: 20px; "></div>
                </div>

                <a id="cambio" class="waves-effect waves-light btn">button</a>
            </div>



            <!--END CONTENT-->

            <!--END PAGE WRAPPER-->

        </div>

        <!--CORE JAVASCRIPT-->
        <script type="text/javascript">
            // ARCADE Software®
            //==================================================================
            $(document).ready(function () {
                datos_grafica_dashboart();
                $('select').material_select();
                $('.datepicker').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15, // Creates a dropdown of 15 years to control year,
                    today: 'Today',
                    clear: 'Clear',
                    close: 'Ok',
                    closeOnSelect: false // Close upon selecting a date,
                });
            }
            );
            //==================================================================
            $(".help").on("click", function () {
                var src = location.href;
                var aux;
                var jsp;
                aux = src.split('/sistema/');
                jsp = aux[1];
                jsp = jsp.replace('.jsp', '');
                window.open("ayuda.jsp?tema=" + jsp, "_blank");
            });
            //==================================================================
            $(".chi").live("click", function () {
                datos_grafica_dashboart('id_boton', '', '');
            });
            //==================================================================
            $(".delete_xxyyzz").live("click", function () {
                var confirma = confirm("PRESIONE ENTER PARA CONTINUAR...");
                if (confirma) {
                    delete_xxyyzz($(this).closest("tr").attr("id"));
                }
            });
            //==================================================================
            Highcharts.chart('container1', {
                chart: {
                    inverted: true,
                    type: 'column'

                },
                title: {
                    text: 'Evaluacion por Reclutador'
                },
                xAxis: {
                    categories: [
                        'Reclutador1',
                        'reclutador2'
                        
                    ]
                },
                yAxis: [{
                        min: 0,
                        title: {
                            text: 'entrevistados'
                        }
                    }, {
                        title: {
                            text: 'Entrevistados'
                        },
                        opposite: true
                    }],
                legend: {
                    shadow: false
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                },
                series: [{
                        name: 'entrevistados',
                        color: 'rgba(165,170,217,1)',
                        data: [150, 73, 20],
                        pointPadding: 0.3,
                        pointPlacement: -0.2
                    }, {
                        name: 'ingresos',
                        color: 'rgba(126,86,134,.9)',
                        data: [140, 90, 40],
                        pointPadding: 0.4,
                        pointPlacement: -0.2
                    }, {
                        name: 'ingresos',
                        color: 'rgba(248,161,63,1)',
                        data: [183.6, 178.8, 198.5],
                        tooltip: {
                            valuePrefix: '$',
                            valueSuffix: ' M'
                        },
                        pointPadding: 0.3,
                        pointPlacement: 0.2,
                        yAxis: 1
                    }, {
                        name: 'entrevistados',
                        color: 'rgba(186,60,61,.9)',
                        data: [203.6, 198.8, 208.5],
                        tooltip: {
                            valuePrefix: '$',
                            valueSuffix: ' M'
                        },
                        pointPadding: 0.4,
                        pointPlacement: 0.2,
                        yAxis: 1
                    }]
            });
            // ARCADE Software®
        </script>
        <!--CORE JAVASCRIPT-->
    </body>
</html>