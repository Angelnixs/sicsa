<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ERP</title>
        <link rel="shortcut icon" href="http://leimihost.com/mx/images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="css/arcade-style-frame.css" media="screen">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JQUERY -->
        <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>       
        <!-- JQUERY -->

        <!--ARCADE JS-->

        <script type="text/javascript" src="js/arcade-alfanumerico.js"></script>
        <script type="text/javascript" src="js/arcade-curp.js"></script>
        <script type="text/javascript" src="js/arcade-fecha.js"></script>
        <script type="text/javascript" src="js/arcade-excel.js"></script>
        <script type="text/javascript" src="js/arcade-sucursal.js"></script>

        <!--materialize-->
        <link rel="stylesheet" type="text/css" href="materialize/css/materialize.css">
        <script type="text/javascript" src="materialize/js/materialize.js"></script>
        <link rel="stylesheet" type="text/css" href="css/arcade-nav.css">
        <!--ARCADE JS-->

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="js/arcade-region.js"></script>
        <script type="text/javascript" src="js/arcade-usuario.js"></script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav id="ayuda_nav" class="grey lighten-1 hide_print" >
                    <div class="nav-wrapper">
                        <div>
                            <a class="ayuda_name brand-logo" style="color:white;">Entradas</a>
                        </div>
                        <ul class="right"  style="margin-right:2%;">    

                            <li><input id="filtro" class="filtro  center-align grey darken-2" type="text"style="width:400px;border-radius: 4px" placeholder="Buscar"></li>
                            <li id="pdf_horas-clase-maestro" style="margin-left: 117px;"><a href="#"><i id="nav_i" class="material-icons help">picture_as_pdf</i></a></li>
                            <li id="roboto">Manuales de usuario de este modulo</li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="container-fluid">
                <ul id="tabs-swipe-demo" class="tabs">
                    <li class="tab col s3"><a class="active" href="#test-swipe-1" style="color:#db040e">ENTRADAS</a></li>
                    <li class="tab col s3"><a href="#test-swipe-2" style="color:#db040e">REPORTE ENTRADAS</a></li>

                </ul>
                <div id="test-swipe-1" class="col s12" style="margin-top:10px;"> 
                    <div class="row center-align">
                        <div class="inpunt-field col s12" style="padding:10px;border-radius: 5px;">
                            <label for="first_name" style="color:#212121;font-size:12px">FECHA</label>
                            <br>
                            <input id="fecha" type="text"class="validate datepicker center-align" style="margin: 0px auto; width: 190px;" >
                        </div>

                        <br><br>
                        <div class="col s10 offset-s1 center-align">
                            <div class="z-depth-2" id="datos_tabla" style="max-height:400px; margin: 0px auto; overflow:auto;margin-bottom:0px; border-radius:4px;"></div>
                            <br>
                            <a class="waves-effect waves-green btn green z-depth-4" id="xlsx" style="border-radius:4px;margin-right:2%; float: right;" onclick="tableToExcel('tabla_entradas', 'LISTADO DE ENTRADAS')" >XLSX</a>
                        </div>
                        <!--                <div class="col s12"  style="padding: 10px;">
                        <a class="waves-effect waves-green btn z-depth-4" id="xlsx" style=" background-color: #02723a;margin: 0px auto;  border-radius:4px;" onclick="tableToExcel('tabla_entradas', 'LISTADO DE ENTRADAS')" >XLSX</a>
                        </div>-->

                    </div>
                </div>
                <div id="test-swipe-2" class="col s12" style="margin-top:10px;">
                    <div class="row">
                        <div class="col s12">
                            <div class="input-field col s2" style="margin-right:10px;" >
                                <select id="id_sucursal" name="id_sucursal"></select>
                                <label>SUCURSAL</label>
                            </div>
                            <div class="input-field col s5" >
                                <input id="fecha2" name="fecha2" type="text" class="validate datepicker" readonly="" style="text-align: center;border: solid 1px gray !important;border-radius: 4px !important;font-weight: bold; width:100px; padding:-5px;margin-bottom: -5px;height: 27px;"></td>

                                <a id="enviar" class="waves-effect waves-light btn blue" style="margin-left: 5px;width: 50px;border-radius: 4px;height:29px;margin-bottom: 5px;"><i class="material-icons" style="margin: -10px;">send</i></a> 
                                <a onclick="tableToExcel('datos_tabla2', 'REPORTE ENTRADAS')" class="waves-effect waves-light btn green" style="margin-left: 5px;width: 50px;border-radius: 4px;height:29px;margin-bottom: 5px;"><i class="material-icons" style="margin: -10px;">explicit</i></a> 

                            </div>
                        </div>
                        <div class="col s12 z-depth-2" style="margin-top:10px;">
                            <table class="striped bordered highligt " style="border: solid 1px #DADADA;">
                                <thead class="hide-on-med-and-down">
                                    <tr style="color: white; background-color: #c70314;">
                                        <th style='text-align:left;width:280px'>TURNO</th>
                                        <th style='text-align:left;width:280px'>USUARIO</th>
                                        <th style='text-align:center;'>LUNES</th>
                                        <th style='text-align:center;'>MARTES</th>
                                        <th style='text-align:center;'>MIERCOLES</th>
                                        <th style='text-align:center;'>JUEVES</th>
                                        <th style='text-align:center;'>VIERNES</th>
                                        <th style='text-align:center;'>SABADO</th>
                                    </tr>
                                </thead>
                                <tbody id="datos_tabla2" class="col s12"  style=" max-height:300px !important;overflow: auto;position: fixed;margin-top:1px;font-size:13px;"></tbody>
                            </table>
                        </div>
                    </div>

                </div>     
            </div>    

            <!--CORE JAVASCRIPT-->
            <script type="text/javascript">
                // ARCADE Software
                //==================================================================
                $(document).ready(
                        function () {
                            fecha_hoy("fecha");
                            select_region_combo_materialize(1, "id_region", true);
                            select_entrada_tabla($("#fecha").val(), "datos_tabla");
                            select_sucursal_combo("id_sucursal", false);
                            $('select').material_select();
                        }
                );
                //==================================================================
                $(".help").on("click", function () {
                    var src = location.href;
                    var aux;
                    var jsp;
                    aux = src.split('/sistema/');
                    jsp = aux[1];
                    jsp = jsp.replace('.jsp', '');
                    window.open("ayuda.jsp?tema=" + jsp, "_blank");
                });
                //==================================================================
                document.querySelector("#filtro").onkeyup = function () {
                $TableFilter("#tabla_entradas", this.value);
            };

            $TableFilter = function (id, value) {
                var rows = document.querySelectorAll(id + ' tbody tr');

                for (var i = 0; i < rows.length; i++) {
                    var showRow = false;

                    var row = rows[i];
                    row.style.display = 'none';

                    for (var x = 0; x < row.childElementCount; x++) {
                        if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                            showRow = true;
                            break;
                        }
                    }

                    if (showRow) {
                        row.style.display = null;
                    }
                }
            };
            //==================================================================

                $('.datepicker').pickadate({
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15, // Creates a dropdown of 15 years to control year,
                    today: 'Today',
                    format: 'yyyy-mm-dd',
                    clear: 'Clear',
                    close: 'Ok',
                    closeOnSelect: false // Close upon selecting a date,
                });
                //==================================================================
                $("#fecha").on("change", function () {
                    select_entrada_tabla($("#fecha").val(), "datos_tabla");
                });
                //==================================================================
                $("#enviar").on("click", function () {
                    select_reporte_entradas($("#id_sucursal").val(), $("#fecha2").val(), "datos_tabla2");
                });
                //==================================================================
                // ARCADE Software
            </script>
            <!--CORE JAVASCRIPT-->
    </body>
</html>